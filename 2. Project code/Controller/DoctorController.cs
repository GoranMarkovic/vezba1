// File:    DoctorController.cs
// Author:  Windows User
// Created: Wednesday, May 20, 2020 10:02:18 PM
// Purpose: Definition of Class DoctorController

using Model;
using Service;
using System;

namespace Controller
{
   public class DoctorController
   {
        private DoctorService doctorService;

        public DoctorController()
        {
            doctorService = new DoctorService();
        }
      
      public void RegisterDoctor(Model.Doctor d)
      {
            doctorService.RegisterDoctor(d);

      }

        public Model.Doctor findDoctorById(String id)
        {
            Doctor doctor = new Doctor();
            doctor = doctorService.findDoctorById(id);
            return doctor;
        }
      
   
   }
}