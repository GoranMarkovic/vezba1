﻿using Model;
using Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZdravoCorporation.Controller
{
    class DirectorController
    {
        private DirectorService directorService;
        private Director director;

        public DirectorController()
        {
            directorService = new DirectorService();
        }

        public void addDirector(Model.Director director)
        {
            directorService.addDirector(director);
        }

        public Model.Director findDirectorById(String id)
        {
            director = directorService.findDirectorById(id);
            return director;
        }

        public void updateDirector(Model.Director director)
        {
            directorService.updateDirector(director);
        }
    }
}
