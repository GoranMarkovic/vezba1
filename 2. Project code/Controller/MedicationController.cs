// File:    MedicationController.cs
// Author:  38162
// Created: Friday, May 15, 2020 9:38:42 PM
// Purpose: Definition of Class MedicationController

using Model;
using Service;
using System;
using System.Collections.Generic;
using System.Windows.Documents;

namespace Controller
{
   public class MedicationController
   {
        private MedicationService medicationService;

        public MedicationController()
        {
            medicationService = new MedicationService();
        }

        public void AddMedication(Model.Medication medication)
        {
            medicationService.AddMedication(medication);
        }
      
        
      public void RemoveMedication(int medicationId)
      {
            medicationService.RemoveMedication(medicationId);
      }

        /*
        public Model.Medication ViewMedication(int medicationId)
        {
           // TODO: implement
           return null;
        }

        public Model.Medication UpdateMedication(Model.Medication medication)
        {
           // TODO: implement
           return null;
        }

        public Boolean OrderMedication(int quantity, int id)
        {
           // TODO: implement
           return null;
        }
        */

        public List<Medication> ViewAllMedications()
      {
            List<Medication> lista = medicationService.ViewAllMedication();
            return lista;
      }
      
        /*
      public Boolean ApproveMedication(int medicationId)
      {
         // TODO: implement
         return null;
      }
      */
   
   }
}