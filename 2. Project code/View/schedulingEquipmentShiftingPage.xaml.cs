﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for schedulingEquipmentShiftingPage.xaml
    /// </summary>
    public partial class schedulingEquipmentShiftingPage : Page
    {
        public schedulingEquipmentShiftingPage()
        {
            InitializeComponent();
        }

        private void TextBox_MouseEnter(object sender, MouseEventArgs e)
        {
            if (sourceTextBox.Text == "search a source room")
                sourceTextBox.Text = "";
        }

        private void TextBox_MouseLeave(object sender, MouseEventArgs e)
        {
            if (sourceTextBox.Text == "")
                sourceTextBox.Text = "search a source room";
        }

        private void TextBox_MouseEnter_1(object sender, MouseEventArgs e)
        {
            if (targetTextBox.Text == "search a target room")
                targetTextBox.Text = "";
        }

        private void TextBox_MouseLeave_1(object sender, MouseEventArgs e)
        {
            if (targetTextBox.Text == "")
                targetTextBox.Text = "search a target room";
        }
    }
}

