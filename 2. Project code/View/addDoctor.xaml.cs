﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for addDoctor.xaml
    /// </summary>
    public partial class addDoctor : Page
    {
        private DoctorController doctorController;
        private Doctor doctor;
        public addDoctor()
        {
            InitializeComponent();
            doctorController = new DoctorController();
            doctor = new Doctor();
            this.DataContext = doctor;


        }

        private void confimNewDoctor(object sender, RoutedEventArgs e)
        {
            //Doctor doctor = new Doctor();
            doctor.Id = 0;
            doctor.Username = username.Text;
            doctor.Password = password.Password;
            doctor.FirstName = firstname.Text;
            doctor.LastName = lastname.Text;
            doctor.Address = address.Text;
            doctor.Phone = Int32.Parse(phone.Text);
            doctor.Jmbg = long.Parse(jmbg.Text);
            doctor.Specialization = Specialization.Cardiologist;
            doctor.Email = email.Text;
            doctor.Birth = birth.Text;

            if(doctorController.findDoctorById(doctor.Id.ToString()).Username == null)
                doctorController.RegisterDoctor(doctor);
            
        }
    }
}
