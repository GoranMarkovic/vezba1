﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for schedulingRenovationPage.xaml
    /// </summary>
    public partial class schedulingRenovationPage : Page
    {
        public schedulingRenovationPage()
        {
            InitializeComponent();
        }

        private void roomTextBox_MouseEnter(object sender, MouseEventArgs e)
        {
            if (roomTextBox.Text == "search a room")
            {
                roomTextBox.Text = "";
            }
        }

        private void roomTextBox_MouseLeave(object sender, MouseEventArgs e)
        {
            if (roomTextBox.Text == "")
            {
                roomTextBox.Text = "search a room";
            }
        }
    }
}
