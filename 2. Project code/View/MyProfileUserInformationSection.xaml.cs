﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZdravoCorporation.Controller;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for MyProfileUserInformationSection.xaml
    /// </summary>
    /// 

    

    public partial class MyProfileUserInformationSection : Page
    {
        private Director director;
        private DirectorController directorController;

        public MyProfileUserInformationSection()
        {
            InitializeComponent();
            directorController = new DirectorController();
            director = directorController.findDirectorById("0");
            this.DataContext = director;
        }
    }
}
