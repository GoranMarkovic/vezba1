﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for addMedication.xaml
    /// </summary>
    public partial class addMedication : Page
    {
        private MedicationController medicationController;

        public addMedication()
        {
            InitializeComponent();
            medicationController = new MedicationController();
        }

        private void addMedicationConfirm(object sender, RoutedEventArgs e)
        {
            Medication medication = new Medication();
            medication.Name = name.Text;
            medication.Ingridients = ingridients.Text;
            medication.Uses = uses.Text;
            medicationController.AddMedication(medication);

            name.Text = "";
            ingridients.Text = "";
            uses.Text = "";
            MessageBox.Show("Uspesno ste dodali lek.");

        }
    }
}
