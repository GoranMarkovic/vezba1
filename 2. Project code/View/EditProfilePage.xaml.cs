﻿using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ZdravoCorporation.Controller;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for EditProfilePage.xaml
    /// </summary>
    public partial class EditProfilePage : Page
    {

        private DirectorController directorController;
        private Director director;
        public EditProfilePage()
        {
            InitializeComponent();

            directorController = new DirectorController();
            director = directorController.findDirectorById("0");
            this.DataContext = director;
        }

        private void confirmProfileChange(object sender, RoutedEventArgs e)
        {
            director.Username = username.Text;
            director.Password = password.Text;
            director.Jmbg = long.Parse(jmbg.Text);
            director.Address = address.Text;
            director.Phone = Int32.Parse(phone.Text);
            director.Email = email.Text;
            director.Birth = birth.Text;

            directorController.updateDirector(director);

            
            
        }
        
    }
}
