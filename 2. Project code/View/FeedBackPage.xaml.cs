﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for FeedBackPage.xaml
    /// </summary>
    public partial class FeedbackPage : Page
    {
        
        //private FeedbackController feedbackController;
        public FeedbackPage()
        {
            InitializeComponent();
            //feedbackController = new FeedbackController();
        }

        /*
        private void ConfirmWrittenFeedback(object sender, RoutedEventArgs e)
        {
            // feedbackController.SaveFeedback(feedbackTextBox.Text);
        }
        */

        private void feedback_Enter(object sender, MouseEventArgs e)
        {
            if (feedbackTextBox.Text == "Enter your thoughts here")
                feedbackTextBox.Text = "";
        }

        private void feedback_Leave(object sender, MouseEventArgs e)
        {
            if (feedbackTextBox.Text == "")
                feedbackTextBox.Text = "Enter your thoughts here";
        }
        
    }
}
