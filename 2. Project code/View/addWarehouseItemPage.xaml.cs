﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for addWarehouseItem.xaml
    /// </summary>
    public partial class addWarehouseItemPage : Page
    {
        public addWarehouseItemPage()
        {
            InitializeComponent();
        }

        private void openAddMedication(object sender, RoutedEventArgs e)
        {
            warehouseItemFrame.Visibility = Visibility.Visible;
            warehouseItemFrame.Content = new addMedication();
        }

        private void openAddConsumable(object sender, RoutedEventArgs e)
        {
            warehouseItemFrame.Visibility = Visibility.Visible;
            warehouseItemFrame.Content = new addConsumable();
        }
    }
}
