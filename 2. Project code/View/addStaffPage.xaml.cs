﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for addStaffPage.xaml
    /// </summary>
    public partial class addStaffPage : Page
    {
        public addStaffPage()
        {
            InitializeComponent();
        }

        private void openAddDoctor(object sender, RoutedEventArgs e)
        {
            addStaffFrame.Visibility = Visibility.Visible;
            addStaffFrame.Content = new addDoctor();
        }

        private void openAddSecretary(object sender, RoutedEventArgs e)
        {
            addStaffFrame.Visibility = Visibility.Visible;
            addStaffFrame.Content = new addSecretary();
        }
    }


}
