﻿using Controller;
using Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ZdravoCorporation.View
{
    /// <summary>
    /// Interaction logic for WarehouseReviewPage.xaml
    /// </summary>
    public partial class WarehouseReviewPage : Page
    {
        private MedicationController medicationController;

        public WarehouseReviewPage()
        {
            InitializeComponent();
            medicationController = new MedicationController();

            var medications = medicationController.ViewAllMedications();
            if (medications.Count > 0)
                ListViewMedications.ItemsSource = medications;
        }

        private void openAddWarehouseItemPage(object sender, RoutedEventArgs e)
        {
            WarehouseFrame.Visibility = Visibility.Visible;
            WarehouseFrame.Content = new addWarehouseItemPage();
        }

       
    }
}
