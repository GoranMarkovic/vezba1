// File:    Answer.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Answer

using System;

namespace Model
{
   public class Answer
   {
      public int Reply()
      {
         // TODO: implement
         return 0;
      }
   
      public Doctor doctor;
      public Question question;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public Question GetQuestion()
      {
         return question;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newQuestion</param>
      public void SetQuestion(Question newQuestion)
      {
         if (this.question != newQuestion)
         {
            if (this.question != null)
            {
               Question oldQuestion = this.question;
               this.question = null;
               oldQuestion.RemoveAnswer(this);
            }
            if (newQuestion != null)
            {
               this.question = newQuestion;
               this.question.AddAnswer(this);
            }
         }
      }
   
      private int id;
      private String title;
      private String text;
      private DateTime date;
   
   }
}