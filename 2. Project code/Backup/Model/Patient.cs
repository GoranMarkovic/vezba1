// File:    Patient.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Patient

using System;

namespace Model
{
   public class Patient : User
   {
      public Intervention[] ViewInterventions()
      {
         // TODO: implement
         return null;
      }
      
      public Intervention ScheduleExamination()
      {
         // TODO: implement
         return null;
      }
      
      public DateTime ChooseTerm()
      {
         // TODO: implement
         return null;
      }
      
      public Doctor ChooseDoctor()
      {
         // TODO: implement
         return null;
      }
      
      public Boolean SetPriotity()
      {
         // TODO: implement
         return null;
      }
      
      public DateTime ChangeTerm()
      {
         // TODO: implement
         return null;
      }
      
      public Doctor ChangeDoctor()
      {
         // TODO: implement
         return null;
      }
      
      public Intervention CancelExamination()
      {
         // TODO: implement
         return null;
      }
      
      public Doctor[] ViewListOfDoctors()
      {
         // TODO: implement
         return null;
      }
      
      public Doctor ViewDoctorsProfile()
      {
         // TODO: implement
         return null;
      }
      
      public Comment CommentOnDoctor()
      {
         // TODO: implement
         return null;
      }
      
      public Boolean RateDoctor()
      {
         // TODO: implement
         return null;
      }
      
      public Boolean EditProfile()
      {
         // TODO: implement
         return null;
      }
   
      public HospitalRoom hospitalRoom;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public HospitalRoom GetHospitalRoom()
      {
         return hospitalRoom;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newHospitalRoom</param>
      public void SetHospitalRoom(HospitalRoom newHospitalRoom)
      {
         if (this.hospitalRoom != newHospitalRoom)
         {
            if (this.hospitalRoom != null)
            {
               HospitalRoom oldHospitalRoom = this.hospitalRoom;
               this.hospitalRoom = null;
               oldHospitalRoom.RemovePatient(this);
            }
            if (newHospitalRoom != null)
            {
               this.hospitalRoom = newHospitalRoom;
               this.hospitalRoom.AddPatient(this);
            }
         }
      }
      public Penalty penalty;
      public System.Collections.ArrayList question;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetQuestion()
      {
         if (question == null)
            question = new System.Collections.ArrayList();
         return question;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetQuestion(System.Collections.ArrayList newQuestion)
      {
         RemoveAllQuestion();
         foreach (Question oQuestion in newQuestion)
            AddQuestion(oQuestion);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddQuestion(Question newQuestion)
      {
         if (newQuestion == null)
            return;
         if (this.question == null)
            this.question = new System.Collections.ArrayList();
         if (!this.question.Contains(newQuestion))
         {
            this.question.Add(newQuestion);
            newQuestion.SetPatient(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveQuestion(Question oldQuestion)
      {
         if (oldQuestion == null)
            return;
         if (this.question != null)
            if (this.question.Contains(oldQuestion))
            {
               this.question.Remove(oldQuestion);
               oldQuestion.SetPatient((Patient)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllQuestion()
      {
         if (question != null)
         {
            System.Collections.ArrayList tmpQuestion = new System.Collections.ArrayList();
            foreach (Question oldQuestion in question)
               tmpQuestion.Add(oldQuestion);
            question.Clear();
            foreach (Question oldQuestion in tmpQuestion)
               oldQuestion.SetPatient((Patient)null);
            tmpQuestion.Clear();
         }
      }
   
      private String bloodType;
      private String allergies;
      private char gender;
      private String meritalStatus;
      private String job;
      private int absenceCount;
   
   }
}