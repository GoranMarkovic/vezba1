// File:    Feedback.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Feedback

using System;

namespace Model
{
   public class Feedback
   {
      public User user;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public User GetUser()
      {
         return user;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newUser</param>
      public void SetUser(User newUser)
      {
         if (this.user != newUser)
         {
            if (this.user != null)
            {
               User oldUser = this.user;
               this.user = null;
               oldUser.RemoveFeedback(this);
            }
            if (newUser != null)
            {
               this.user = newUser;
               this.user.AddFeedback(this);
            }
         }
      }
   
      private int id;
      private DateTime date;
      private String text;
   
   }
}