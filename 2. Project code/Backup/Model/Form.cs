// File:    Form.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Form

using System;

namespace Model
{
   public class Form
   {
      public Intervention intervention;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public Intervention GetIntervention()
      {
         return intervention;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newIntervention</param>
      public void SetIntervention(Intervention newIntervention)
      {
         if (this.intervention != newIntervention)
         {
            if (this.intervention != null)
            {
               Intervention oldIntervention = this.intervention;
               this.intervention = null;
               oldIntervention.RemoveForm(this);
            }
            if (newIntervention != null)
            {
               this.intervention = newIntervention;
               this.intervention.AddForm(this);
            }
         }
      }
   
      private DateTime date;
      private String name;
      private String hospital_name;
      private int prescriptionId;
      private String signature;
      private String description;
   
   }
}