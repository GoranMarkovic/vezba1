// File:    User.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class User

using System;

namespace Model
{
   public class User : NotLoggedInUser
   {
      public Boolean ViewProfile()
      {
         // TODO: implement
         return null;
      }
   
      public System.Collections.ArrayList feedback;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetFeedback()
      {
         if (feedback == null)
            feedback = new System.Collections.ArrayList();
         return feedback;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetFeedback(System.Collections.ArrayList newFeedback)
      {
         RemoveAllFeedback();
         foreach (Feedback oFeedback in newFeedback)
            AddFeedback(oFeedback);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddFeedback(Feedback newFeedback)
      {
         if (newFeedback == null)
            return;
         if (this.feedback == null)
            this.feedback = new System.Collections.ArrayList();
         if (!this.feedback.Contains(newFeedback))
         {
            this.feedback.Add(newFeedback);
            newFeedback.SetUser(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveFeedback(Feedback oldFeedback)
      {
         if (oldFeedback == null)
            return;
         if (this.feedback != null)
            if (this.feedback.Contains(oldFeedback))
            {
               this.feedback.Remove(oldFeedback);
               oldFeedback.SetUser((User)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllFeedback()
      {
         if (feedback != null)
         {
            System.Collections.ArrayList tmpFeedback = new System.Collections.ArrayList();
            foreach (Feedback oldFeedback in feedback)
               tmpFeedback.Add(oldFeedback);
            feedback.Clear();
            foreach (Feedback oldFeedback in tmpFeedback)
               oldFeedback.SetUser((User)null);
            tmpFeedback.Clear();
         }
      }
      public System.Collections.ArrayList notification;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetNotification()
      {
         if (notification == null)
            notification = new System.Collections.ArrayList();
         return notification;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetNotification(System.Collections.ArrayList newNotification)
      {
         RemoveAllNotification();
         foreach (Notification oNotification in newNotification)
            AddNotification(oNotification);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddNotification(Notification newNotification)
      {
         if (newNotification == null)
            return;
         if (this.notification == null)
            this.notification = new System.Collections.ArrayList();
         if (!this.notification.Contains(newNotification))
         {
            this.notification.Add(newNotification);
            newNotification.SetUser(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveNotification(Notification oldNotification)
      {
         if (oldNotification == null)
            return;
         if (this.notification != null)
            if (this.notification.Contains(oldNotification))
            {
               this.notification.Remove(oldNotification);
               oldNotification.SetUser((User)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllNotification()
      {
         if (notification != null)
         {
            System.Collections.ArrayList tmpNotification = new System.Collections.ArrayList();
            foreach (Notification oldNotification in notification)
               tmpNotification.Add(oldNotification);
            notification.Clear();
            foreach (Notification oldNotification in tmpNotification)
               oldNotification.SetUser((User)null);
            tmpNotification.Clear();
         }
      }
   
      private int password;
      private UserPosition position;
      private int id;
      private String name;
      private String surname;
      private String address;
      private String email;
      private int jmbg;
      private String birth;
      private int phone;
      private int username;
      private int role;
   
   }
}