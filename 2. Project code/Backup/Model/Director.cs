// File:    Director.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Director

using System;

namespace Model
{
   public class Director : User
   {
      public User RegisterStaff()
      {
         // TODO: implement
         return null;
      }
      
      public void MakeRenovation()
      {
         // TODO: implement
      }
      
      public void ManipulateMedication()
      {
         // TODO: implement
      }
      
      public void ManipulateConsumable()
      {
         // TODO: implement
      }
      
      public void ChangeRoomInfo()
      {
         // TODO: implement
      }
      
      public void TransferStaticEquipment()
      {
         // TODO: implement
      }
   
   }
}