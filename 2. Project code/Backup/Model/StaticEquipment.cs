// File:    StaticEquipment.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class StaticEquipment

using System;

namespace Model
{
   public class StaticEquipment : Equipment
   {
      public HospitalRoom hospitalRoom;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public HospitalRoom GetHospitalRoom()
      {
         return hospitalRoom;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newHospitalRoom</param>
      public void SetHospitalRoom(HospitalRoom newHospitalRoom)
      {
         if (this.hospitalRoom != newHospitalRoom)
         {
            if (this.hospitalRoom != null)
            {
               HospitalRoom oldHospitalRoom = this.hospitalRoom;
               this.hospitalRoom = null;
               oldHospitalRoom.RemoveStaticEquipment(this);
            }
            if (newHospitalRoom != null)
            {
               this.hospitalRoom = newHospitalRoom;
               this.hospitalRoom.AddStaticEquipment(this);
            }
         }
      }
   
      private String name;
   
   }
}