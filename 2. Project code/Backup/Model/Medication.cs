// File:    Medication.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Medication

using System;

namespace Model
{
   public class Medication
   {
      public void ViewInfo()
      {
         // TODO: implement
      }
   
      public System.Collections.ArrayList approvedByDoctors;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetApprovedByDoctors()
      {
         if (approvedByDoctors == null)
            approvedByDoctors = new System.Collections.ArrayList();
         return approvedByDoctors;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetApprovedByDoctors(System.Collections.ArrayList newApprovedByDoctors)
      {
         RemoveAllApprovedByDoctors();
         foreach (Doctor oDoctor in newApprovedByDoctors)
            AddApprovedByDoctors(oDoctor);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddApprovedByDoctors(Doctor newDoctor)
      {
         if (newDoctor == null)
            return;
         if (this.approvedByDoctors == null)
            this.approvedByDoctors = new System.Collections.ArrayList();
         if (!this.approvedByDoctors.Contains(newDoctor))
            this.approvedByDoctors.Add(newDoctor);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveApprovedByDoctors(Doctor oldDoctor)
      {
         if (oldDoctor == null)
            return;
         if (this.approvedByDoctors != null)
            if (this.approvedByDoctors.Contains(oldDoctor))
               this.approvedByDoctors.Remove(oldDoctor);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllApprovedByDoctors()
      {
         if (approvedByDoctors != null)
            approvedByDoctors.Clear();
      }
   
      private int id;
      private String name;
      private String ingridients;
      private int uses;
      private Boolean approved = False;
      private int approvingConuter;
      private int quantity;
      private int dose;
   
   }
}