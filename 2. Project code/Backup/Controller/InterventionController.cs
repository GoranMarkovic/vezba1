// File:    InterventionController.cs
// Author:  Windows User
// Created: Wednesday, May 20, 2020 9:59:50 PM
// Purpose: Definition of Class InterventionController

using System;

namespace Controller
{
   public class InterventionController
   {
      public Model.Intervention ScheduleIntervention(Model.Intervention intervention)
      {
         // TODO: implement
         return null;
      }
      
      public Model.Intervention ScheduleEmergencyIntervention(Model.Intervention intervention)
      {
         // TODO: implement
         return null;
      }
      
      public Boolean CancelIntervention(int interventionId)
      {
         // TODO: implement
         return null;
      }
      
      public Model.Intervention ViewAllInterventions(int patientId)
      {
         // TODO: implement
         return null;
      }
      
      public Model.Intervention ViewScheduledInterventions(int doctorId)
      {
         // TODO: implement
         return null;
      }
      
      public Model.Intervention UpdateIntervention(Model.Intervention intervention)
      {
         // TODO: implement
         return null;
      }
      
      public Intervention[] ViewInterventionsByRoom(int roomId)
      {
         // TODO: implement
         return null;
      }
      
      public Model.Intervention ViewIntervention(int interventionId)
      {
         // TODO: implement
         return null;
      }
      
      public Model.Priority ChoosePriority(Model.Priority priority)
      {
         // TODO: implement
         return null;
      }
      
      public DateTime ChooseDate(DateTime date)
      {
         // TODO: implement
         return null;
      }
      
      public DateTime ChooseTime(DateTime time)
      {
         // TODO: implement
         return null;
      }
      
      public DateTime[] ViewAvaliableDatesForDoctor(int doctorID)
      {
         // TODO: implement
         return null;
      }
      
      public DateTime[ ViewAvaliableTermsForDoctorAndDate(int doctorID, DateTime date)
      {
         // TODO: implement
         return null;
      }
   
   }
}