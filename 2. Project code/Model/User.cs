// File:    User.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class User

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Model
{

    public enum UserPosition { DIRECTOR, DOCTOR, SECRETARY };

    public class User :  ObservableObject
   {

        private String password;
        private UserPosition position;
        private int id;
        private String firstName;
        private String lastName;
        private String address;
        private String email;
        private long jmbg;
        private String birth;
        private int phone;
        private String username;

        private void ValidateProperty<T>(T value, string name)
        {
            Validator.ValidateProperty(value, new ValidationContext(this, null, null)
            {
                MemberName = name
            });
        }

        [Required(ErrorMessage = "Must not be empty.")]
        public String FirstName
        {
            get { return firstName; }
            set
            {
                if (firstName != value)
                {
                    ValidateProperty(value, "FirstName");
                    OnPropertyChanged(ref username, value);
                    firstName = value;
                    //OnPropertyChanged("FirstName");
                    //OnPropertyChanged("Fullname");
                }
            }
        }


        [Required(ErrorMessage = "Must not be empty.")]
        public String LastName
        {
            get { return lastName; }
            set
            {
                if (lastName != value)
                {
                    lastName = value;
                    //OnPropertyChanged("LastName");
                    //OnPropertyChanged("Fullname");
                }
            }
        }

        public String Address
        {
            get { return address; }
            set
            {
                if (address != value)
                {
                    address = value;
                    //OnPropertyChanged("Address");
                }
            }
        }

        [Required(ErrorMessage = "Must not be empty.")]
        [RegularExpression("^[a-zA-Z0-9_.-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$", ErrorMessage = "Must be a valid email")]
        public String Email
        {
            get { return email; }
            set
            {
                if (email != value)
                {
                    ValidateProperty(value, "Email");
                    OnPropertyChanged(ref email, value);
                    email = value;
                    //OnPropertyChanged("FirstName");
                }
            }
        }

        public UserPosition Position
        {
            get { return position; }
            set
            {
                if (position != value)
                {
                    position = value;
                    //OnPropertyChanged("Position");
                }
            }
        }


        [Required(ErrorMessage = "Must not be empty.")]
        [StringLength(15, MinimumLength = 5, ErrorMessage = "Must be at least 5 characters.")]
        public String Username
        {
            get { return username; }
            set
            {
                if (username != value)
                {
                    username = value;
                    //OnPropertyChanged("Username");
                }
            }
        }

        public int Phone
        {
            get { return phone; }
            set
            {
                if (phone != value)
                {
                    phone = value;
                    //OnPropertyChanged("Phone");
                }
            }
        }

        public String Birth
        {
            get { return birth; }
            set
            {
                if (birth != value)
                {
                    birth = value;
                    //OnPropertyChanged("Birth");
                }
            }
        }

        
        //[Required]
        //[StringLength(50, MinimumLength = 8, ErrorMessage = "Must be at least 8 characters.")]
        public String Password
        {
            get { return password; }
            set
            {
                if (password != value)
                {
                    ValidateProperty(value, "Password");
                    OnPropertyChanged(ref password, value);
                    password = value;
                    //OnPropertyChanged("Password");
                }
            }
        }

        public long Jmbg
        {
            get { return jmbg; }
            set
            {
                if (jmbg != value)
                {
                    jmbg = value;
                    //OnPropertyChanged("Jmbg");
                }
            }
        }

        public int Id
        {
            get { return id; }
            set
            {
                if(id != value)
                {
                    id = value;
                    //OnPropertyChanged("Id");
                }
            }
        }

        //public event PropertyChangedEventHandler PropertyChanged;

        //private void OnPropertyChanged(String propertyName)
        //{
        //    if (PropertyChanged != null)
        //    {
        //        PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        //    }
        //}
    }


}
