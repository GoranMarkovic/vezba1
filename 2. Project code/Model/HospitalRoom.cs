// File:    HospitalRoom.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class HospitalRoom

using System;

namespace Model
{
   public class HospitalRoom
   {
      public void ViewRoomInfo()
      {
         // TODO: implement
      }
   
      public System.Collections.ArrayList staticEquipment;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetStaticEquipment()
      {
         if (staticEquipment == null)
            staticEquipment = new System.Collections.ArrayList();
         return staticEquipment;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetStaticEquipment(System.Collections.ArrayList newStaticEquipment)
      {
         RemoveAllStaticEquipment();
         foreach (StaticEquipment oStaticEquipment in newStaticEquipment)
            AddStaticEquipment(oStaticEquipment);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddStaticEquipment(StaticEquipment newStaticEquipment)
      {
         if (newStaticEquipment == null)
            return;
         if (this.staticEquipment == null)
            this.staticEquipment = new System.Collections.ArrayList();
         if (!this.staticEquipment.Contains(newStaticEquipment))
         {
            this.staticEquipment.Add(newStaticEquipment);
            newStaticEquipment.SetHospitalRoom(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveStaticEquipment(StaticEquipment oldStaticEquipment)
      {
         if (oldStaticEquipment == null)
            return;
         if (this.staticEquipment != null)
            if (this.staticEquipment.Contains(oldStaticEquipment))
            {
               this.staticEquipment.Remove(oldStaticEquipment);
               oldStaticEquipment.SetHospitalRoom((HospitalRoom)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllStaticEquipment()
      {
         if (staticEquipment != null)
         {
            System.Collections.ArrayList tmpStaticEquipment = new System.Collections.ArrayList();
            foreach (StaticEquipment oldStaticEquipment in staticEquipment)
               tmpStaticEquipment.Add(oldStaticEquipment);
            staticEquipment.Clear();
            foreach (StaticEquipment oldStaticEquipment in tmpStaticEquipment)
               oldStaticEquipment.SetHospitalRoom((HospitalRoom)null);
            tmpStaticEquipment.Clear();
         }
      }
      public System.Collections.ArrayList notAvailablePeriod;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetNotAvailablePeriod()
      {
         if (notAvailablePeriod == null)
            notAvailablePeriod = new System.Collections.ArrayList();
         return notAvailablePeriod;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetNotAvailablePeriod(System.Collections.ArrayList newNotAvailablePeriod)
      {
         RemoveAllNotAvailablePeriod();
         foreach (NotAvailablePeriod oNotAvailablePeriod in newNotAvailablePeriod)
            AddNotAvailablePeriod(oNotAvailablePeriod);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddNotAvailablePeriod(NotAvailablePeriod newNotAvailablePeriod)
      {
         if (newNotAvailablePeriod == null)
            return;
         if (this.notAvailablePeriod == null)
            this.notAvailablePeriod = new System.Collections.ArrayList();
         if (!this.notAvailablePeriod.Contains(newNotAvailablePeriod))
         {
            this.notAvailablePeriod.Add(newNotAvailablePeriod);
            newNotAvailablePeriod.SetHospitalRoom(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveNotAvailablePeriod(NotAvailablePeriod oldNotAvailablePeriod)
      {
         if (oldNotAvailablePeriod == null)
            return;
         if (this.notAvailablePeriod != null)
            if (this.notAvailablePeriod.Contains(oldNotAvailablePeriod))
            {
               this.notAvailablePeriod.Remove(oldNotAvailablePeriod);
               oldNotAvailablePeriod.SetHospitalRoom((HospitalRoom)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllNotAvailablePeriod()
      {
         if (notAvailablePeriod != null)
         {
            System.Collections.ArrayList tmpNotAvailablePeriod = new System.Collections.ArrayList();
            foreach (NotAvailablePeriod oldNotAvailablePeriod in notAvailablePeriod)
               tmpNotAvailablePeriod.Add(oldNotAvailablePeriod);
            notAvailablePeriod.Clear();
            foreach (NotAvailablePeriod oldNotAvailablePeriod in tmpNotAvailablePeriod)
               oldNotAvailablePeriod.SetHospitalRoom((HospitalRoom)null);
            tmpNotAvailablePeriod.Clear();
         }
      }
      public System.Collections.ArrayList patient;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetPatient()
      {
         if (patient == null)
            patient = new System.Collections.ArrayList();
         return patient;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetPatient(System.Collections.ArrayList newPatient)
      {
         RemoveAllPatient();
         foreach (Patient oPatient in newPatient)
            AddPatient(oPatient);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddPatient(Patient newPatient)
      {
         if (newPatient == null)
            return;
         if (this.patient == null)
            this.patient = new System.Collections.ArrayList();
         if (!this.patient.Contains(newPatient))
         {
            this.patient.Add(newPatient);
            newPatient.SetHospitalRoom(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemovePatient(Patient oldPatient)
      {
         if (oldPatient == null)
            return;
         if (this.patient != null)
            if (this.patient.Contains(oldPatient))
            {
               this.patient.Remove(oldPatient);
               oldPatient.SetHospitalRoom((HospitalRoom)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllPatient()
      {
         if (patient != null)
         {
            System.Collections.ArrayList tmpPatient = new System.Collections.ArrayList();
            foreach (Patient oldPatient in patient)
               tmpPatient.Add(oldPatient);
            patient.Clear();
            foreach (Patient oldPatient in tmpPatient)
               oldPatient.SetHospitalRoom((HospitalRoom)null);
            tmpPatient.Clear();
         }
      }
      public System.Collections.ArrayList renovation;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetRenovation()
      {
         if (renovation == null)
            renovation = new System.Collections.ArrayList();
         return renovation;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetRenovation(System.Collections.ArrayList newRenovation)
      {
         RemoveAllRenovation();
         foreach (Renovation oRenovation in newRenovation)
            AddRenovation(oRenovation);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddRenovation(Renovation newRenovation)
      {
         if (newRenovation == null)
            return;
         if (this.renovation == null)
            this.renovation = new System.Collections.ArrayList();
         if (!this.renovation.Contains(newRenovation))
         {
            this.renovation.Add(newRenovation);
            newRenovation.SetHospitalRoom(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveRenovation(Renovation oldRenovation)
      {
         if (oldRenovation == null)
            return;
         if (this.renovation != null)
            if (this.renovation.Contains(oldRenovation))
            {
               this.renovation.Remove(oldRenovation);
               oldRenovation.SetHospitalRoom((HospitalRoom)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllRenovation()
      {
         if (renovation != null)
         {
            System.Collections.ArrayList tmpRenovation = new System.Collections.ArrayList();
            foreach (Renovation oldRenovation in renovation)
               tmpRenovation.Add(oldRenovation);
            renovation.Clear();
            foreach (Renovation oldRenovation in tmpRenovation)
               oldRenovation.SetHospitalRoom((HospitalRoom)null);
            tmpRenovation.Clear();
         }
      }
      public System.Collections.ArrayList intervention;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetIntervention()
      {
         if (intervention == null)
            intervention = new System.Collections.ArrayList();
         return intervention;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetIntervention(System.Collections.ArrayList newIntervention)
      {
         RemoveAllIntervention();
         foreach (Intervention oIntervention in newIntervention)
            AddIntervention(oIntervention);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddIntervention(Intervention newIntervention)
      {
         if (newIntervention == null)
            return;
         if (this.intervention == null)
            this.intervention = new System.Collections.ArrayList();
         if (!this.intervention.Contains(newIntervention))
         {
            this.intervention.Add(newIntervention);
            newIntervention.SetHospitalRoom(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveIntervention(Intervention oldIntervention)
      {
         if (oldIntervention == null)
            return;
         if (this.intervention != null)
            if (this.intervention.Contains(oldIntervention))
            {
               this.intervention.Remove(oldIntervention);
               oldIntervention.SetHospitalRoom((HospitalRoom)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllIntervention()
      {
         if (intervention != null)
         {
            System.Collections.ArrayList tmpIntervention = new System.Collections.ArrayList();
            foreach (Intervention oldIntervention in intervention)
               tmpIntervention.Add(oldIntervention);
            intervention.Clear();
            foreach (Intervention oldIntervention in tmpIntervention)
               oldIntervention.SetHospitalRoom((HospitalRoom)null);
            tmpIntervention.Clear();
         }
      }
   
      private int id;
      private InterventionType type;
      private String name;
      private int floor;
      private int numberOfFreeBeds;
   
   }
}