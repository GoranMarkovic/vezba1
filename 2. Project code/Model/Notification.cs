// File:    Notification.cs
// Author:  vlada
// Created: Wednesday, May 27, 2020 10:57:30 AM
// Purpose: Definition of Class Notification

using System;

namespace Model
{
   public class Notification
   {
      public User user;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public User GetUser()
      {
         return user;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newUser</param>
      public void SetUser(User newUser)
      {
         if (this.user != newUser)
         {
            if (this.user != null)
            {
               User oldUser = this.user;
               this.user = null;
               oldUser.RemoveNotification(this);
            }
            if (newUser != null)
            {
               this.user = newUser;
               this.user.AddNotification(this);
            }
         }
      }
   
      private DateTime date;
      private String content;
      private String receiver;
      private int medicationId;
   
   }
}