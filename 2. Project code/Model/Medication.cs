// File:    Medication.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Medication

using System;
using System.Collections.Generic;
using ZdravoCorporation.Repository;

namespace Model
{
    public class Medication
    {
        private MedicationIdRepository medicationIdRepository;

        private int id;
        private String name;
        private String ingridients;
        private String uses;
        private Boolean approved = false;
        private int approvingConuter;
        private int quantity = 5;
        private int dose;

        public Medication()
        {
            medicationIdRepository = new MedicationIdRepository();
        }

        public int Id
        {
            get { return id; }
            set
            {
                if (value != id)
                {
                    id = value;
                }
            }
        }
        public String Name
        {
            get { return name; }
            set
            {
                if (value != name)
                {
                    name = value;
                }
            }
        }
        public String Ingridients
        {
            get { return ingridients; }
            set
            {
                if (value != ingridients)
                {
                    ingridients = value;
                }
            }
        }
        public String Uses
        {
            get { return uses; }
            set
            {
                if (value != uses)
                {
                    uses = value;
                }
            }
        }

        public int medicationIdSetter()
        {
            List<int> idsList = new List<int>();
            idsList = medicationIdRepository.findAllMedicationid();
            if (idsList.Count == 0) // ukoliko je lista prazna prvi id je 0
            {
                medicationIdRepository.addMedicationId(0);
                return 0;
            }
            else
            {
                int id;
                Random random = new Random();
                id = random.Next(1, 100);
                while (idsList.Contains(id))
                {
                    id = random.Next(1, 100);
                }
                medicationIdRepository.addMedicationId(id);
                return id;
            }
        }


    }
}