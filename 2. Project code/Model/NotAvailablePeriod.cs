// File:    NotAvailablePeriod.cs
// Author:  38162
// Created: Friday, May 15, 2020 11:58:22 AM
// Purpose: Definition of Class NotAvailablePeriod

using System;

namespace Model
{
   public class NotAvailablePeriod
   {
      public HospitalRoom hospitalRoom;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public HospitalRoom GetHospitalRoom()
      {
         return hospitalRoom;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newHospitalRoom</param>
      public void SetHospitalRoom(HospitalRoom newHospitalRoom)
      {
         if (this.hospitalRoom != newHospitalRoom)
         {
            if (this.hospitalRoom != null)
            {
               HospitalRoom oldHospitalRoom = this.hospitalRoom;
               this.hospitalRoom = null;
               oldHospitalRoom.RemoveNotAvailablePeriod(this);
            }
            if (newHospitalRoom != null)
            {
               this.hospitalRoom = newHospitalRoom;
               this.hospitalRoom.AddNotAvailablePeriod(this);
            }
         }
      }
   
      private DateTime startPeriod;
      private DateTime endPeriod;
   
   }
}