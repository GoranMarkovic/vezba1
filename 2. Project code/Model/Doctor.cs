// File:    Doctor.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Doctor

using System;
using System.Security.Policy;

namespace Model
{
    public enum Specialization { Cardiologist, Dermatologist};
   public class Doctor : User
   {
     
        private DateTime startWorking;
        private DateTime endWorking;
        private Specialization specialization;

        public DateTime StartWorking
        {
            get { return startWorking; }
            set
            {
                if(startWorking != value)
                {
                    startWorking = value;
                }
            }
        }

        public DateTime EndWorking
        {
            get { return endWorking; }
            set
            {
                if (endWorking != value)
                {
                    endWorking = value;
                }
            }
        }

        public Specialization Specialization
        {
            get { return specialization; }
            set
            {
                if (specialization != value)
                {
                    specialization = value;
                }
            }
        }

    }
}