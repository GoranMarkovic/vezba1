// File:    Therapy.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Therapy

using System;

namespace Model
{
   public class Therapy : Form
   {
      public System.Collections.ArrayList medication;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetMedication()
      {
         if (medication == null)
            medication = new System.Collections.ArrayList();
         return medication;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetMedication(System.Collections.ArrayList newMedication)
      {
         RemoveAllMedication();
         foreach (Medication oMedication in newMedication)
            AddMedication(oMedication);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddMedication(Medication newMedication)
      {
         if (newMedication == null)
            return;
         if (this.medication == null)
            this.medication = new System.Collections.ArrayList();
         if (!this.medication.Contains(newMedication))
            this.medication.Add(newMedication);
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveMedication(Medication oldMedication)
      {
         if (oldMedication == null)
            return;
         if (this.medication != null)
            if (this.medication.Contains(oldMedication))
               this.medication.Remove(oldMedication);
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllMedication()
      {
         if (medication != null)
            medication.Clear();
      }
   
   }
}