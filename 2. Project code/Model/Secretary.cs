// File:    Secretary.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Secretary

using System;

namespace Model
{
   public class Secretary : User
   {
      public User RegisterPatient()
      {
         // TODO: implement
         return null;
      }
      
      public Intervention ScheduleIntervention()
      {
         // TODO: implement
         return null;
      }
      
      public Intervention CancelIntervention()
      {
         // TODO: implement
         return null;
      }
      
      public HospitalRoom BookRoom()
      {
         // TODO: implement
         return null;
      }
      
      public Notification CancelationInform()
      {
         // TODO: implement
         return null;
      }
      
      public Intervention[] ViewInterventions()
      {
         // TODO: implement
         return null;
      }
      
      public Doctor ChangeDoctor()
      {
         // TODO: implement
         return null;
      }
      
      public DateTime ChangeTerm()
      {
         // TODO: implement
         return null;
      }
      
      public penalty PunishPatient()
      {
         // TODO: implement
         return null;
      }
      
      public HospitalRoom[] ViewRoomOccupancy()
      {
         // TODO: implement
         return null;
      }
      
      public Notification TermInformPatient()
      {
         // TODO: implement
         return null;
      }
      
      public Notification DoctorInformPatient()
      {
         // TODO: implement
         return null;
      }
   
   }
}