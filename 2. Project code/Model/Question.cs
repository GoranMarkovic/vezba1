// File:    Question.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Question

using System;

namespace Model
{
   public class Question
   {
      public int Post()
      {
         // TODO: implement
         return 0;
      }
   
      public System.Collections.ArrayList answer;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetAnswer()
      {
         if (answer == null)
            answer = new System.Collections.ArrayList();
         return answer;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetAnswer(System.Collections.ArrayList newAnswer)
      {
         RemoveAllAnswer();
         foreach (Answer oAnswer in newAnswer)
            AddAnswer(oAnswer);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddAnswer(Answer newAnswer)
      {
         if (newAnswer == null)
            return;
         if (this.answer == null)
            this.answer = new System.Collections.ArrayList();
         if (!this.answer.Contains(newAnswer))
         {
            this.answer.Add(newAnswer);
            newAnswer.SetQuestion(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveAnswer(Answer oldAnswer)
      {
         if (oldAnswer == null)
            return;
         if (this.answer != null)
            if (this.answer.Contains(oldAnswer))
            {
               this.answer.Remove(oldAnswer);
               oldAnswer.SetQuestion((Question)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllAnswer()
      {
         if (answer != null)
         {
            System.Collections.ArrayList tmpAnswer = new System.Collections.ArrayList();
            foreach (Answer oldAnswer in answer)
               tmpAnswer.Add(oldAnswer);
            answer.Clear();
            foreach (Answer oldAnswer in tmpAnswer)
               oldAnswer.SetQuestion((Question)null);
            tmpAnswer.Clear();
         }
      }
      public Patient patient;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public Patient GetPatient()
      {
         return patient;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newPatient</param>
      public void SetPatient(Patient newPatient)
      {
         if (this.patient != newPatient)
         {
            if (this.patient != null)
            {
               Patient oldPatient = this.patient;
               this.patient = null;
               oldPatient.RemoveQuestion(this);
            }
            if (newPatient != null)
            {
               this.patient = newPatient;
               this.patient.AddQuestion(this);
            }
         }
      }
   
      private int id;
      private String title;
      private String text;
      private DateTime date;
   
   }
}