// File:    Intervention.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 9:35:21 PM
// Purpose: Definition of Class Intervention

using System;

namespace Model
{
   public class Intervention
   {
      public Doctor doctor;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public Doctor GetDoctor()
      {
         return doctor;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newDoctor</param>
      public void SetDoctor(Doctor newDoctor)
      {
         if (this.doctor != newDoctor)
         {
            if (this.doctor != null)
            {
               Doctor oldDoctor = this.doctor;
               this.doctor = null;
               oldDoctor.RemoveIntervention(this);
            }
            if (newDoctor != null)
            {
               this.doctor = newDoctor;
               this.doctor.AddIntervention(this);
            }
         }
      }
      public HospitalRoom hospitalRoom;
      
      /// <pdGenerated>default parent getter</pdGenerated>
      public HospitalRoom GetHospitalRoom()
      {
         return hospitalRoom;
      }
      
      /// <pdGenerated>default parent setter</pdGenerated>
      /// <param>newHospitalRoom</param>
      public void SetHospitalRoom(HospitalRoom newHospitalRoom)
      {
         if (this.hospitalRoom != newHospitalRoom)
         {
            if (this.hospitalRoom != null)
            {
               HospitalRoom oldHospitalRoom = this.hospitalRoom;
               this.hospitalRoom = null;
               oldHospitalRoom.RemoveIntervention(this);
            }
            if (newHospitalRoom != null)
            {
               this.hospitalRoom = newHospitalRoom;
               this.hospitalRoom.AddIntervention(this);
            }
         }
      }
      public Patient patient;
      public System.Collections.ArrayList form;
      
      /// <pdGenerated>default getter</pdGenerated>
      public System.Collections.ArrayList GetForm()
      {
         if (form == null)
            form = new System.Collections.ArrayList();
         return form;
      }
      
      /// <pdGenerated>default setter</pdGenerated>
      public void SetForm(System.Collections.ArrayList newForm)
      {
         RemoveAllForm();
         foreach (Form oForm in newForm)
            AddForm(oForm);
      }
      
      /// <pdGenerated>default Add</pdGenerated>
      public void AddForm(Form newForm)
      {
         if (newForm == null)
            return;
         if (this.form == null)
            this.form = new System.Collections.ArrayList();
         if (!this.form.Contains(newForm))
         {
            this.form.Add(newForm);
            newForm.SetIntervention(this);      
         }
      }
      
      /// <pdGenerated>default Remove</pdGenerated>
      public void RemoveForm(Form oldForm)
      {
         if (oldForm == null)
            return;
         if (this.form != null)
            if (this.form.Contains(oldForm))
            {
               this.form.Remove(oldForm);
               oldForm.SetIntervention((Intervention)null);
            }
      }
      
      /// <pdGenerated>default removeAll</pdGenerated>
      public void RemoveAllForm()
      {
         if (form != null)
         {
            System.Collections.ArrayList tmpForm = new System.Collections.ArrayList();
            foreach (Form oldForm in form)
               tmpForm.Add(oldForm);
            form.Clear();
            foreach (Form oldForm in tmpForm)
               oldForm.SetIntervention((Intervention)null);
            tmpForm.Clear();
         }
      }
   
      private int id;
      private String description;
      private Boolean absence;
      private InterventionType type;
      private DateTime start;
      private DateTime end;
      private Priority priority;
   
   }
}