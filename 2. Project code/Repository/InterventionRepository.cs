// File:    InterventionRepository.cs
// Author:  Andjela Paunovic
// Created: Monday, May 25, 2020 4:23:48 PM
// Purpose: Definition of Class InterventionRepository

using System;

namespace Repository
{
   public class InterventionRepository
   {
      public Intervention[] FindAllInterventionsByDoctor(int doctorId)
      {
         // TODO: implement
         return null;
      }
      
      public Intervention AddIntervention(Intervention intervention)
      {
         // TODO: implement
         return null;
      }
      
      public IIntervention DeleteIntervention(int interventionId)
      {
         // TODO: implement
         return null;
      }
      
      public Intervention UpdateIntervention(Intervention intervention)
      {
         // TODO: implement
         return null;
      }
      
      public Intervention[] FindAllInterventionsByPatient(int patientId)
      {
         // TODO: implement
         return null;
      }
      
      public Intervention[] FindAllInterventionsByRoom(int roomId)
      {
         // TODO: implement
         return null;
      }
      
      public Intervention FindIntervention(int interventionId)
      {
         // TODO: implement
         return null;
      }
      
      public DateTime[] FindAvaliableDatesForDoctor(int doctorId)
      {
         // TODO: implement
         return null;
      }
      
      public DateTime[] FindAvaliableTermsForDoctorAndDate(int doctorId, int date)
      {
         // TODO: implement
         return null;
      }
   
   }
}