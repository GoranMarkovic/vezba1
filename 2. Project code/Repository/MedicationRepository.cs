// File:    MedicationRepository.cs
// Author:  38162
// Created: Friday, May 15, 2020 9:41:49 PM
// Purpose: Definition of Class MedicationRepository

using Model;
using System;
using System.Collections.Generic;
using System.IO;
using ZdravoCorporation.Repository;

namespace Repository
{
   public class MedicationRepository
   {
        private MedicationIdRepository medicationIdRepository;

        public MedicationRepository()
        {
            medicationIdRepository = new MedicationIdRepository();
        }

      public void AddMedication(Model.Medication medication)
      {
            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/medications.csv", true))
            {
                string[] values = { medication.Id.ToString(), medication.Name, medication.Ingridients, medication.Uses };
                string line = String.Join(";", values);
                writer.WriteLine(line);
            }
        }

        public List<Medication> FindAllMedication()
        {
            string line;
            List<Medication> lista = new List<Medication>();

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/medications.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    Medication medication = new Medication();
                    medication.Id = Int32.Parse(line.Split(';')[0]);
                    medication.Name = line.Split(';')[1];
                    medication.Ingridients = line.Split(';')[2];
                    medication.Uses = line.Split(';')[3];
                    lista.Add(medication);
                }
            }

            return lista;
        }

        public void RemoveMedication(int medicationId)
        {
            medicationIdRepository.RemoveMedicationId(medicationId);

            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/ConsoleApp1/Files/medications.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] != medicationId.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/ConsoleApp1/Files/medications.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }

        /*

        public Model.Medication FindMedicationById(int medicationId)
      {
         // TODO: implement
         return null;
      }
      
      public Model.Medication UpdateMedication(int medicationId)
      {
         // TODO: implement
         return null;
      }
      
      public Medication[] FindAllMedication()
      {
         // TODO: implement
         return null;
      }
      */

    }
}