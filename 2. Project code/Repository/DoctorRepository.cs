// File:    DoctorRepository.cs
// Author:  Hristina
// Created: Wednesday, May 27, 2020 6:24:11 PM
// Purpose: Definition of Class DoctorRepository

using Model;
using System;
using System.Collections.Generic;
using System.IO;

namespace Repository
{
   public class DoctorRepository
   {
      
      public List<Doctor> FindAllDoctors()
      {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/doctor.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    lines.Add(line);
                }
            }

            List<Doctor> doctors = new List<Doctor>();
            
            foreach(String linee in lines)
            {
                Doctor doctor = new Doctor();
                doctor.Id = Int32.Parse(linee.Split(';')[0]);
                doctor.Username = linee.Split(';')[1];
                doctor.FirstName = linee.Split(';')[2];
                doctor.LastName = linee.Split(';')[3];
                doctor.Password = linee.Split(';')[4];
                doctor.Phone = Int32.Parse(linee.Split(';')[5]);
                doctor.Jmbg = long.Parse(linee.Split(';')[6]);
                doctor.Position = 0;
                doctor.Address = linee.Split(';')[8];
                doctor.Birth = linee.Split(';')[9];
                doctor.Email = linee.Split(';')[10];
                doctors.Add(doctor);
            }

            return doctors;

            
        }

        public void AddDoctor(Model.Doctor doctor)
        {

            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/doctor.csv", true))
            {
                string[] values = { doctor.Id.ToString(), doctor.Username, doctor.FirstName, doctor.LastName, doctor.Password, doctor.Phone.ToString(), doctor.Jmbg.ToString(), doctor.Position.ToString(), doctor.Address, doctor.Birth, doctor.Email };
                string line = String.Join(";", values);
                writer.WriteLine(line);
            }

        }

        public Model.Doctor findDoctorById(String id)
        {
            var strLines = File.ReadLines(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/doctor.csv");
            Doctor doctor = new Doctor();

            foreach (var line in strLines)
            {
                if (line.Split(';')[0].Equals(id))
                {
                    doctor.Id = Int32.Parse(line.Split(';')[0]);
                    doctor.Username = line.Split(';')[1];
                    doctor.FirstName = line.Split(';')[2];
                    doctor.LastName = line.Split(';')[3];
                    doctor.Password = line.Split(';')[4];
                    doctor.Phone = Int32.Parse(line.Split(';')[5]);
                    doctor.Jmbg = long.Parse(line.Split(';')[6]);
                    doctor.Position = 0;
                    doctor.Address = line.Split(';')[8];
                    doctor.Birth = line.Split(';')[9];
                    doctor.Email = line.Split(';')[10];
                    break;
                }
            }
            return doctor;
        }

        public void updateDoctor(Model.Doctor d)
        {
            Doctor doctor = new Doctor();
            doctor = findDoctorById(d.Id.ToString());
            deleteDoctor(doctor);
            AddDoctor(d);

        }

        public void deleteDoctor(Model.Doctor d)
        {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/doctor.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[1] == d.Id.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/doctor.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }

    }
}