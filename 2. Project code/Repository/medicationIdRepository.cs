﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZdravoCorporation.Repository
{
    class MedicationIdRepository
    {
        public void addMedicationId(int medicationId)
        {
            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/medicationId.csv", true))
            {
                string line = medicationId.ToString() + ";";
                writer.WriteLine(line);
            }
        }

        public List<int> findAllMedicationid()
        {
            var strLines = File.ReadLines(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/medicationId.csv");
            List<int> lista = new List<int>();

            foreach (var line in strLines)
            {
                lista.Add(Int32.Parse(line.Split(';')[0]));
            }
            return lista;
        }

        public void RemoveMedicationId(int medicationId)
        {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/medicationId.csv"))
            {
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[0] != medicationId.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/medicationId.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }
    }
}
