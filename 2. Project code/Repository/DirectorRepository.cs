// File:    DirectorRepository.cs
// Author:  Hristina
// Created: Wednesday, May 27, 2020 6:24:12 PM
// Purpose: Definition of Class DirectorRepository

using Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Documents;

namespace Repository
{
    public class DirectorRepository
    {
        public void AddDirector(Model.Director director)
        {

            using (var writer = new StreamWriter(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/director.csv", true))
            {
                string[] values = { director.Id.ToString(), director.Username, director.FirstName, director.LastName, director.Password, director.Phone.ToString(), director.Jmbg.ToString(), director.Position.ToString(), director.Address, director.Birth, director.Email };
                string line = String.Join(";", values);
                writer.WriteLine(line);
            }

        }

        public Model.Director findDirectorById(String id)
        {
            var strLines = File.ReadLines(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/director.csv");
            Director director = new Director();

            foreach (var line in strLines)
            {
                if (line.Split(';')[0].Equals(id))
                {
                    director.Id = Int32.Parse(line.Split(';')[0]);
                    director.Username = line.Split(';')[1];
                    director.FirstName = line.Split(';')[2];
                    director.LastName = line.Split(';')[3];
                    director.Password = line.Split(';')[4];
                    director.Phone = Int32.Parse(line.Split(';')[5]);
                    director.Jmbg = long.Parse(line.Split(';')[6]);
                    director.Position = 0;
                    director.Address = line.Split(';')[8];
                    director.Birth = line.Split(';')[9];
                    director.Email = line.Split(';')[10];
                    break;
                }
            }
            return director;
        }

        public void updateDirector(Model.Director d)
        {
            Director director = new Director();
            director = findDirectorById(d.Id.ToString());
            deleteDirector(director);
            AddDirector(d);

        }

        public void deleteDirector(Model.Director d)
        {
            List<String> lines = new List<string>();
            string line;

            using (System.IO.StreamReader file = new System.IO.StreamReader(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/director.csv"))
            { 
                while ((line = file.ReadLine()) != null)
                {
                    if (line.Split(';')[1] == d.Id.ToString())
                    {
                        lines.Add(line);
                    }
                }
            }

            using (System.IO.StreamWriter outfile = new System.IO.StreamWriter(@"C:/Users/38162/Desktop/Projekat/SIMS projekat/Lokalni repozitorijum/projekat2/project/zdravoCorporationBackend/Files/director.csv"))
            {
                outfile.Write(String.Join(System.Environment.NewLine, lines.ToArray()));
            }
        }

    }
}

