// File:    DirectorService.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 11:00:42 PM
// Purpose: Definition of Class DirectorService

using Model;
using Repository;
using System;

namespace Service
{
   public class DirectorService
   {
        DirectorRepository directorRepository;
        

      public DirectorService()
      {
          directorRepository = new DirectorRepository();
      }

      public Boolean addDirector(Model.Director d)
        {
            directorRepository.AddDirector(d);

            return true;
        }

        public Model.Director findDirectorById(String id)
        {
            Director director = new Director();
            director = directorRepository.findDirectorById(id);
            return director;
        }
      
        public void updateDirector(Model.Director director)
        {
            directorRepository.updateDirector(director);
        }
   
   }
}