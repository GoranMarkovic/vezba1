// File:    DoctorService.cs
// Author:  Hristina
// Created: Wednesday, May 13, 2020 10:39:53 PM
// Purpose: Definition of Class DoctorService

using Model;
using Repository;
using System;

namespace Service
{
   public class DoctorService
   {

        private DoctorRepository doctorRepository;

        public DoctorService()
        {
            doctorRepository = new DoctorRepository();
        }

        public void RegisterDoctor(Model.Doctor doctor)
        {
            doctorRepository.AddDoctor(doctor);
        }
      
        public Model.Doctor findDoctorById(String id)
        {
            Doctor doctor = new Doctor();
            doctor = doctorRepository.findDoctorById(id);
            return doctor;
        }
   
   }
}