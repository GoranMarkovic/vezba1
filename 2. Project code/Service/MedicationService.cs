// File:    MedicationService.cs
// Author:  38162
// Created: Friday, May 15, 2020 9:33:09 PM
// Purpose: Definition of Class MedicationService

using Model;
using Repository;
using System;
using System.Collections.Generic;
using System.Windows.Documents;

namespace Service
{
   public class MedicationService
   {
        private MedicationRepository medicationRepository;

        public MedicationService()
        {
            medicationRepository = new MedicationRepository();
        }

        public void AddMedication(Model.Medication medication)
        {
            medication.Id = medication.medicationIdSetter();
            medicationRepository.AddMedication(medication);
        }

        
      public void RemoveMedication(int medicationId)
      {
            medicationRepository.RemoveMedication(medicationId);
      }

        /*
        public Model.Medication ViewMedication(int medicationId)
        {
           // TODO: implement
           return null;
        }

        public Model.Medication UpdateMedication(int medicationId)
        {
           // TODO: implement
           return null;
        }

        public Boolean OrderMedication(int quantity)
        {
           // TODO: implement
           return null;
        }
        */

        public List<Medication> ViewAllMedication()
      {
            List<Medication> lista = medicationRepository.FindAllMedication();
            return lista;
      }
      
        /*
      public Model.Medication FindMedication(String name)
      {
         // TODO: implement
         return null;
      }
      */

    }
}